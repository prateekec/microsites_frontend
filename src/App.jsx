import { useState } from "react";
import "./reset.css";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import viteLogo from "/vite.svg";
import "./App.css";
import LoginPage from "./modules/login/components/loginPage/LoginPage";
import { ChakraProvider } from "@chakra-ui/react";
import MicrositesPage from "./modules/microsites/components/MicrositesPage/MicrositesPage";
import OpenUrlsPage from "./modules/vendorUrls/components/OpenUrlsPage";
import CmsHomePage from "./modules/cms/component/CmsHomePage/CmsHomePage";
import Otis from "./modules/templates/talentCommunityForm/Otis/Otis";
import MasterCard from "./modules/templates/talentCommunityForm/MasterCard/MasterCard";
import PageBuilder from "./modules/pageBuilder/components/PageBuilder/PageBuilder";
import WordPress from "./modules/templates/talentCommunityForm/Wordpress/Wordpress";
import WordpressThank from "./modules/templates/talentCommunityForm/Wordpress/WordpressThank";
import Hosting from "./modules/templates/talentCommunityForm/Grapes/GrapesJSRenderedComponent";
import { useSelector } from "react-redux";
function App() {
    return (
        <ChakraProvider>
            <BrowserRouter>
                <>
                    <Routes>
                        <Route path="/" element={<MicrositesPage />}></Route>
                        <Route path="/cms/:siteType" element={<CmsHomePage />}></Route>
                        <Route path="/pagebuilder/:type/:id" element={<PageBuilder />}></Route>

                        <Route path="/auth/login" element={<LoginPage />}></Route>

                        <Route path="/openUrls" element={<OpenUrlsPage></OpenUrlsPage>}></Route>
                        <Route path="/mastercard" element={<MasterCard></MasterCard>}></Route>
                        <Route path="/wordpress" element={<WordPress />}></Route>
                        <Route path="/wordpressThank" element={<WordpressThank />}></Route>
                        <Route path="/otis" element={<Otis></Otis>}></Route>
                        <Route path="/:user_id/:template_id" element={<Hosting />}></Route>
                    </Routes>
                </>
            </BrowserRouter>
        </ChakraProvider>
    );
}

export default App;
