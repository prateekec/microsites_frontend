import { createSlice } from "@reduxjs/toolkit";

const initialState = { data: {}, loaded: false, error: { status: false, message: "" } };

const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        setUser(state, action) {
            state.data = action.payload;
            state.loaded = true;
            state.error = initialState.error;
        },
        setError(state, action) {
            state.error.status = true;
            state.error.message = action.payload;
        },
        removeUser(state, action) {
            return initialState;
        },
    },
});

const { reducer: userReducer, actions } = userSlice;

export const userActions = actions;
export default userReducer;
