import { Navigate } from "react-router-dom";
import CmsTabs from "../CmsTabs/CmsTabs";
import CmsTabsContent from "../CmsTabsContent/CmsTabsContent";
import Header from "../Header/Header";
import ModuleContainer from "../ModuleContainer/ModuleContainer";

import "./CmsHomePage.css";
import { useSelector } from "react-redux";
function CmsHomePage() {
    const user = useSelector((state) => state.user);
    if (user.loaded) {
        return (
            <>
                <div className="cms-home-page-outer">
                    <div className="cms-home-page-inner">
                        <Header></Header>
                        <div className="cms-main-container">
                            <CmsTabs></CmsTabs>
                            <CmsTabsContent></CmsTabsContent>
                            <ModuleContainer></ModuleContainer>
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return <Navigate to={"/auth/login"}></Navigate>;
    }
}
export default CmsHomePage;
