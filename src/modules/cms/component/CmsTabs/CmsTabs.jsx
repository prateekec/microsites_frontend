import "./CmsTabs.css";

function CmsTabs() {
    return (
        <div className="cms-tabs-outer">
            <div className="cms-tabs-inner">
                <div className="single-tab">Modules</div>
            </div>
        </div>
    );
}

export default CmsTabs;
