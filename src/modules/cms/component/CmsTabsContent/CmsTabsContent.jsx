import "./CmsTabsContent.css";

function CmsTabsContent() {
    return (
        <div className="cms-tabs-content-outer">
            <div className="cms-tabs-content-inner">
                <div className="item">Open Jobs</div>
                <div className="item">Internal Jobs</div>
                <div className="item">Single Jobs</div>
                <div className="item">Recommended Jobs</div>
                <div className="item">Talent Community Form</div>
                <div className="item">Internal Projects/Gigs</div>
            </div>
        </div>
    );
}

export default CmsTabsContent;
