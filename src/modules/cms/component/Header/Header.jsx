import { EditIcon } from "@chakra-ui/icons";

import "./Header.css";
import { Avatar } from "@chakra-ui/react";
import { useSelector } from "react-redux";
function Header() {
    const user = useSelector((state) => state.user);
    return (
        <div className="header-outer">
            <div className="header-inner">
                <div className="left">
                    <div className="site-name">
                        Client's site
                        <EditIcon ml={"0.7rem"}></EditIcon>
                    </div>
                    <div className="searchbar">Search</div>
                </div>
                <div className="right">
                    <Avatar name={user.data.firstname} />
                </div>
            </div>
        </div>
    );
}
export default Header;
