import { Box, Button, Flex, Image, Text } from "@chakra-ui/react";
import Otis, { OtisDesign } from "../../../templates/talentCommunityForm/Otis/Otis";
import "./ModuleContainer.css";
import { MasterCardDesign } from "../../../templates/talentCommunityForm/MasterCard/MasterCard";
import { useEffect, useState } from "react";
import GrapesJSRenderedComponent from "../../../templates/talentCommunityForm/Grapes/GrapesJSRenderedComponent";
import axios from "axios";

import OtisPng from "../../../../assets/otis.png";
import html2canvas from "html2canvas";
import grapesjs from "grapesjs";
import { useSelector } from "react-redux";
import { Navigate, useNavigate } from "react-router-dom";
import newTemplateJsonData from "./newTemplate.json";

function ModuleContainer() {
    const user = useSelector((state) => state.user);
    const [designId, setDesignId] = useState("");
    const [templates, setTemplates] = useState([]);
    const [template, setTemplate] = useState();
    const [loaded, setLoaded] = useState(false);
    const navigate = useNavigate();
    useEffect(() => {
        loadUserTemplates();
    }, [templates.length]);
    // console.log(user);

    async function loadDefaultTemplates() {
        const res = await axios({
            method: "get",
            url: "http://localhost:7171/api/get_default_templates",
            params: {
                user_id: 123,
                page: 1,
            },
            headers: {
                Authorization: user.data.access_token,
            },
        });
        setTemplates(res.data.data);
    }
    async function loadUserTemplates() {
        const res = await axios({
            method: "get",
            url: "http://localhost:7171/api/get_user_templates",
            params: {
                user_id: user.data._id,
                page: 1,
            },
            headers: {
                Authorization: user.data.access_token,
            },
        });
        // console.log(res.data.data);
        setTemplates(res.data.data);
    }
    async function openPagebuilder(type, id, copy_id) {
        if (id == "new") {
            let reqData = {
                user_id: user.data._id,

                template_name: "main",
                pages: [{ title: "main", html: "", css: "", js: "" }],
                project_json: newTemplateJsonData,
            };
            console.log(reqData);
            const res = await axios({
                url: "http://localhost:7171/api/save_user_template",
                method: "POST",
                data: reqData,
                headers: {
                    Authorization: user.data.access_token,
                },
            });
            // console.log(res.data.data._id);
            id = res.data.data._id;
        }

        navigate(`/pagebuilder/${type}/${id}?copy=${copy_id}`);
    }
    async function deleteTemplate(id) {
        try {
            const res = await axios({
                url: "http://localhost:7171/api/delete_user_template",
                method: "DELETE",
                data: {
                    template_id: id,
                },
                headers: {
                    Authorization: user.data.access_token,
                },
            });
            loadDefaultTemplates();
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div className="module-container-outer">
            <div className="module-container-inner">
                <div className="module-header-outer">
                    <div className="module-header-inner">
                        <div className="templates">Templates</div>
                        <Flex justifyContent={"space-between"}>
                            <Flex gap={"1rem"}>
                                <Button onClick={() => loadDefaultTemplates()}>Default Templates</Button>
                                <Button onClick={() => loadUserTemplates()}>Your Designs</Button>
                                <Button onClick={() => openPagebuilder("user", "new")}>Create</Button>
                            </Flex>
                            {!template ? null : (
                                <>
                                    <Button onClick={() => openPagebuilder("user", "new", template._id)} backgroundColor={"blue"} color={"white"}>
                                        Edit as new
                                    </Button>
                                    <Button onClick={() => openPagebuilder(template.template_type, template._id)} backgroundColor={"blue"} color={"white"}>
                                        Edit
                                    </Button>
                                    <Button onClick={() => deleteTemplate(template._id)} backgroundColor={"red"} color={"white"}>
                                        Delete
                                    </Button>
                                </>
                            )}
                        </Flex>
                    </div>
                </div>
                <Flex>
                    <Box height={"88vh"} flexBasis={"13rem"} background={"#F4E0E0"} padding={"1rem"} overflow={"scroll"}>
                        <Flex flexDir={"column"} gap={"1.5rem"}>
                            {templates.map((template) => {
                                return (
                                    <div key={template._id}>
                                        <Box border={designId == template.template_id ? "2px solid black" : null} overflow={"hidden"}>
                                            <Image
                                                designid={template.template_id}
                                                onClick={() => {
                                                    if (template.user_id) {
                                                        return setTemplate({ ...template, template_type: "user" });
                                                    }
                                                    return setTemplate({ ...template, template_type: "default" });
                                                }}
                                                height={"200Px"}
                                                width={"200px"}
                                                src={template.pages[0].js}
                                            ></Image>
                                        </Box>
                                    </div>
                                );
                            })}
                        </Flex>
                    </Box>
                    <div className="module-content">
                        <div className="design-container">{template ? <Image src={template.pages[0].js}></Image> : null}</div>
                    </div>
                </Flex>
            </div>
        </div>
    );
}
export default ModuleContainer;
