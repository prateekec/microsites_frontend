import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Route } from "react-router-dom";
import LoginPage from "../loginPage/LoginPage";
function ProtectedRoute({ path, element }) {
    const user = useSelector((state) => state.user);

    if (user.loaded) {
        return <Route path={path} element={element} />;
    } else {
        return <Navigate to="/auth/login" />;
    }
}

export default ProtectedRoute;
