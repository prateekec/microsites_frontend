import "./LoginPage.css";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Flex, InputRightElement, useToast } from "@chakra-ui/react";
import { Input, InputGroup } from "@chakra-ui/react";
import { userActions } from "../../../../features/userSlice";
import { useDispatch } from "react-redux";

function LoginPage() {
    const toast = useToast();
    const navigate = useNavigate();
    const [formData, setFormData] = useState({});
    const [formError, setFormError] = useState({ status: false, message: "" });
    const [isSignIn, setIsSignIn] = useState(true);
    const dispatch = useDispatch();

    const handleInputChange = (event) => {
        console.log(event.target.value);
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };
    const handleSignIn = async (event) => {
        event.preventDefault();
        if (validateFormData(formData, setFormError, isSignIn)) {
            try {
                const response = await axios.post(`http://localhost:7171/api/login`, formData, {
                    headers: {
                        "Content-Type": "application/json",
                    },
                    withCredentials: true,
                });
                dispatch(userActions.setUser(response.data.data));
                navigate("/");
            } catch (error) {
                console.log(error);
                if (error.response) {
                    console.log("FJLDJK");
                    setFormError({ status: true, message: error.response.data.error });
                } else {
                    setFormError({ status: true, message: error.message });
                }
            }
        }
    };

    const handleSignUp = async (event) => {
        event.preventDefault();
        if (validateFormData(formData, setFormError, isSignIn)) {
            try {
                const response = await axios.post(`http://localhost:7171/api/signup`, formData, {
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                toast({
                    title: "Account created.",
                    description: "We've create your account.",
                    status: "success",
                    duration: 9000,
                    isClosable: true,
                });
                dispatch(userActions.setUser(response.data.data));
                navigate("/");
            } catch (error) {
                if (error.response.data) {
                    setFormError({ status: true, message: error.response.data.error });
                } else {
                    setFormError({ status: true, message: error.message });
                }
            }
        }
    };
    function handleGoogleLogin() {
        // Redirect to the server route for Google OAuth login
        // window.location.href = `${BASE_URL}/api/users/auth/google`;
    }

    return (
        <div className="login-outer">
            <div className="login-inner">
                {isSignIn ? (
                    <SignInForm handleInputChange={handleInputChange} handleSignIn={handleSignIn} formError={formError} formData={formData}></SignInForm>
                ) : (
                    <SignUpForm handleInputChange={handleInputChange} handleSignUp={handleSignUp} formError={formError} formData={formData}></SignUpForm>
                )}

                <div className="signIn-signUp">
                    {isSignIn ? "New User?" : "Already a user?"}
                    <p
                        onClick={() => {
                            setFormError({ status: false, message: "" });
                            setIsSignIn(!isSignIn);
                        }}
                    >
                        {isSignIn ? "Sign up" : "Sign in"}
                    </p>
                </div>

                <Flex color=" #8993a4" width={"95%"} gap={"0.3rem"} alignItems={"center"} margin={"1rem auto"} fontSize={"12px"}>
                    <Box w={"100%"} height={"1px"} background={"#8993a4"}></Box>Or
                    <Box w={"100%"} height={"1px"} background={"#8993a4"}></Box>
                </Flex>

                <div className="google-button" onClick={handleGoogleLogin}>
                    <img src="https://static.naukimg.com/s/5/105/i/ic-google.png"></img>
                    <span>Sign in with Google</span>
                </div>
            </div>
        </div>
    );
}
function validateFormData(formData, setFormError, isSignIn) {
    if (!formData.email) {
        setFormError({ status: true, message: "Please enter your email!" });
        return false;
    }
    if (!validate_email(formData.email)) {
        setFormError({ status: true, message: "Email is not valid!" });
        return false;
    }
    if (isSignIn == false) {
        if (!formData.firstname) {
            setFormError({ status: true, message: "Please enter your first name!" });
            return false;
        }
        if (!formData.lastname) {
            setFormError({ status: true, message: "Please enter your last name!" });
            return false;
        }
        if (!validate_name(formData.name)) {
            setFormError({ status: true, message: "Please enter a valid name!" });
            return false;
        }
        if (!formData.password) {
            setFormError({ status: true, message: "Please enter password!" });
            return false;
        }
        if (!formData.rePassword) {
            setFormError({ status: true, message: "Please confirm your password!" });
            return false;
        }
        if (!validate_password(formData.password, formData.rePassword)) {
            setFormError({ status: true, message: "Password did not match!" });
            return false;
        }
    }
    setFormError({ status: false, message: "" });
    return true;
}
function validate_email(email) {
    let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (regex.test(email)) {
        return true;
    } else {
        return false;
    }
}
function validate_password(password, rePassword) {
    if (password && rePassword && password != rePassword) {
        return false;
    } else {
        return true;
    }
}

function validate_name(name) {
    let regex = /^[a-zA-Z]+(?:[-\s'][a-zA-Z]+)*$/;
    if (regex.test(name)) {
        return true;
    } else {
        return false;
    }
}

function SignInForm({ handleInputChange, handleSignIn, formError, formData }) {
    return (
        <>
            <div className="form-heading">Sign in to your account</div>
            <form id="form-signIn" action="/" onSubmit={handleSignIn}>
                {formError.status ? <p className="error">{formError.message}</p> : null}
                <input id="email" name="email" type="text" placeholder="Email" onChange={handleInputChange}></input>
                <PasswordInput handleInputChange={handleInputChange}></PasswordInput>
                <button type="submit">Sign in</button>
            </form>
        </>
    );
}

function SignUpForm({ handleInputChange, handleSignUp, formError, formData }) {
    return (
        <>
            <div className="form-heading">Register yourself !</div>
            <form id="form-signUp" action="/" onSubmit={handleSignUp}>
                {formError.status ? <p className="error">{formError.message}</p> : null}
                <input id="firstname" name="firstname" type="text" placeholder="Enter your first name" onChange={handleInputChange}></input>
                <input id="lastname" name="lastname" type="text" placeholder="Enter your last name" onChange={handleInputChange}></input>
                <input id="email" name="email" type="text" placeholder="Enter your email" onChange={handleInputChange}></input>
                <PasswordInput handleInputChange={handleInputChange}></PasswordInput>
                <input id="rePassword" name="rePassword" type="password" placeholder="Confirm password" onChange={handleInputChange}></input>
                <button type="submit">Register</button>
            </form>
        </>
    );
}
function PasswordInput({ handleInputChange }) {
    const [show, setShow] = useState(false);
    const handleClick = () => setShow(!show);

    return (
        <InputGroup size="md" border={"white"}>
            <Input
                id="password"
                name="password"
                pr="4.5rem"
                type={show ? "text" : "password"}
                placeholder="Enter password"
                onChange={(e) => {
                    handleInputChange(e);
                    // console.log(e);
                }}
            />
            <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={handleClick}>
                    {show ? "Hide" : "Show"}
                </Button>
            </InputRightElement>
        </InputGroup>
    );
}
export default LoginPage;
