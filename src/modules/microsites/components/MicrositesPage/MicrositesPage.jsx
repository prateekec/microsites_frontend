import { Link, Navigate } from "react-router-dom";
import "./MicrositesPage.css";
import { useSelector } from "react-redux";

function MicrositesPage() {
    const user = useSelector((state) => state.user);
    if (user.loaded) {
        return (
            <div className="microsites-page-outer">
                <div className="microsites-page-inner">
                    <h1>Acme's Sites</h1>
                    <div className="box-container">
                        <div className="box">
                            <Link to="cms/career_site">Career Site including Open Jobs and Event pages</Link>
                        </div>
                        <div className="box">
                            <Link to="cms/internal_mobility">Internal Mobility</Link>
                        </div>
                        <div className="box">
                            <Link to="cms/employee_referrals">Employee Referrals</Link>
                        </div>
                        <div className="box">
                            <Link to="cms/custom">Custom</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return <Navigate to="/auth/login"></Navigate>;
    }
}

export default MicrositesPage;
