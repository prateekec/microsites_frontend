import React, { useEffect, useState } from "react";
import grapesjs from "grapesjs";
import "grapesjs/dist/css/grapes.min.css";
import "./PageBuilder.css";
import gjsForms from "grapesjs-plugin-forms";
import plugin from "grapesjs-blocks-basic";

import "../../../../../node_modules/grapesjs-preset-webpage/dist/index.js";
import axios from "axios";
import html2canvas from "html2canvas";
import { useLocation, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import testData from "../../../../../test.json";
import { Flex } from "@chakra-ui/react";
import { getEditorInstance, initEditor, setPlugins } from "../../editor_services/grapesjsEditor.js";

function handleEditorEvents(type, id, copy_id, user, setPages, setSelected) {
    return (editor) => {
        editor.on("update", async () => {
            try {
                let pagesArray = [];
                for (let page of editor.Pages.getAll()) {
                    const pm = editor.Pages;
                    if (pm.getSelected().id == page.id) {
                        const html = editor.getHtml();
                        const css = editor.getCss();
                        const js = editor.getJs();
                        const canvas = await html2canvas(editor.Canvas.getDocument().body);
                        const imageUrl = canvas.toDataURL("image/png");
                        pagesArray.push({ title: page.get("name"), html: html, css: css, js: imageUrl });
                    } else {
                        pagesArray.push({ title: page.get("name") });
                    }
                }
                const projectData = editor.getProjectData();

                let reqData = {
                    user_id: user.data._id,

                    template_name: "First Template",
                    pages: pagesArray,
                    project_json: projectData,
                };
                if (id != "new") {
                    reqData = { ...reqData, template_id: id };
                }

                const res = await axios({
                    url: "http://localhost:7171/api/save_user_template",
                    method: "POST",
                    data: reqData,
                    headers: {
                        Authorization: user.data.access_token,
                    },
                });
                reqData = { ...reqData, template_id: res.data.data._id };
                // console.log(res);
            } catch (error) {
                console.log(error);
            }
        });
        editor.on("load", async () => {
            if (copy_id == "undefined") {
                let url;
                if (type == "user") {
                    url = "http://localhost:7171/api/get_user_template";
                } else {
                    url = "http://localhost:7171/api/get_default_template";
                }
                try {
                    const res = await axios({
                        url: url,
                        method: "GET",
                        headers: {
                            Authorization: user.data.access_token,
                        },
                        params: {
                            template_id: id,
                        },
                    });
                    // console.log(res.data.data);

                    editor.loadProjectData(res.data.data.project_json);
                } catch (error) {
                    console.log("dffsdf", error.message);
                }
            } else {
                try {
                    let url = "http://localhost:7171/api/get_user_template";
                    const res = await axios({
                        url: url,
                        method: "GET",
                        headers: {
                            Authorization: user.data.access_token,
                        },
                        params: {
                            template_id: copy_id,
                        },
                    });
                    // console.log(res.data.data);

                    editor.loadProjectData(res.data.data.project_json);
                } catch (error) {
                    console.log(error);
                }
            }
            editor.UndoManager.clear();
            setPages(editor.Pages.getAll());
            setSelected(editor.Pages.getSelected().id);
        });
    };
}

function PageBuilder() {
    const { type, id } = useParams();
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const copy_id = queryParams.get("copy");
    const user = useSelector((state) => state.user);
    const [pages, setPages] = useState([]);
    const [loaded, setLoaded] = useState();
    const [selected, setSelected] = useState();

    async function initGrapesJs() {
        setPlugins(["grapesjs-preset-webpage", plugin, gjsForms, handleEditorEvents(type, id, copy_id, user, setPages, setSelected)]);
        initEditor();
        const editor = getEditorInstance();

        setLoaded(true);
    }
    useEffect(() => {
        initGrapesJs();
    }, []);

    function addPage() {
        const editor = getEditorInstance();
        const pageManager = editor.Pages;
        const newPage = pageManager.add({
            name: "Untitled-" + (pageManager.getAll().length + 1),
        });
        const um = editor.UndoManager;
        um.add(editor.getConfig());
        editor.refresh();

        setPages(pageManager.getAll());
        // console.log(editor.Pages.getAll());
    }

    function selectPage(id) {
        const editor = getEditorInstance();
        editor.Pages.select(id);
        setSelected(editor.Pages.getSelected().id);
    }
    function removePage(Page) {
        const editor = getEditorInstance();
        editor.Pages.remove(Page.id);

        setPages((pages) => pages.filter((page) => page.id !== Page.id));
    }

    return (
        <>
            <Flex>
                <div className="app-wrap">
                    <div className="pages-wrp">
                        <div className="add-page" onClick={addPage}>
                            Add new page
                        </div>
                        <div className="pages">
                            {pages.map((page) => {
                                return (
                                    <div onClick={() => selectPage(page.id)} key={page.id} className={selected == page.id ? "page selected" : "page"}>
                                        {page.get("name")}
                                        {selected !== page.id && (
                                            <span onClick={() => removePage(page)} className="page-close">
                                                &times;
                                            </span>
                                        )}
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className="editor-wrap">
                        <div id="gjs"></div>
                    </div>
                </div>
                <div id="gjs"></div>;
            </Flex>
        </>
    );
}
export default PageBuilder;
