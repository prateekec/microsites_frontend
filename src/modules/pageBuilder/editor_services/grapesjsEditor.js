import grapesjs from "grapesjs";
import "grapesjs/dist/css/grapes.min.css";
import "../../../../node_modules/grapesjs-preset-webpage/dist/index.js";

let editorInstance = null;
let plugins = [];
export const setPlugins = (Plugins) => {
    plugins = Plugins;
};

export const initEditor = () => {
    editorInstance = grapesjs.init({
        container: "#gjs",
        height: "100vh",
        width: "100%",

        plugins: plugins,

        storageManager: {
            id: "gjs-",
            type: "local",
            autosave: true,
            storeComponents: true,
            storeStyles: true,
            storeHtml: true,
            storeCss: true,
        },
        deviceManager: {
            devices: [
                {
                    id: "desktop",
                    name: "Desktop",
                    width: "",
                },
                {
                    id: "tablet",
                    name: "Tablet",
                    width: "768px",
                    widthMedia: "992px",
                },
                {
                    id: "mobilePortrait",
                    name: "Mobile portrait",
                    width: "320px",
                    widthMedia: "575px",
                },
            ],
        },
        blockManager: {
            blocks: [
                {
                    id: "my-block", // the ID of your block
                    label: "My Block", // the label that appears in the block manager
                    content: '<div class="my-block">This is a custom block</div>', // the HTML content of the block
                    category: "Custom",
                },
            ],
        },
    });
};

// Add more functions as needed
export const getEditorInstance = () => editorInstance;
