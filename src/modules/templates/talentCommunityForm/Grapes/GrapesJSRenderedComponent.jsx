import axios from "axios";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const Hosting = () => {
    const { user_id, template_id } = useParams();
    useEffect(() => {
        const renderPage = async () => {
            try {
                const res = await axios({
                    url: "http://localhost:7171/api/get_user_template",
                    method: "GET",
                    params: {
                        template_id: template_id,
                    },
                    headers: {
                        Authorization:
                            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjU3YTllNzNhNTJkNTEyYmYyYWU1NzI0IiwiaWF0IjoxNzAzMDY1MTk3fQ.9I48HaFNpzP1KwDeF0xNU7YrIxJ1sA30odGQjxI_bcY",
                    },
                });
                // console.log(res.data.data[1]);
                const data = res.data.data.pages;
                // console.log(data.js);

                // Dynamically inject the CSS into the head of the document
                const styleElement = document.createElement("style");
                styleElement.innerHTML = data[0].css;
                document.head.appendChild(styleElement);

                // Set the HTML content to a container element
                const container = document.getElementById("grapesjs-container" + template_id);
                if (container) {
                    container.innerHTML = data[0].html;

                    const form = container.querySelector("form");

                    if (form) {
                        // Add a submit event listener to the form
                        form.addEventListener("submit", async (event) => {
                            event.preventDefault(); // Prevent the default form submission

                            const formData = new FormData(form);

                            // Log form data to the console (you can modify this part)
                            const formDataJSON = { user_id: user_id };
                            formData.forEach((value, key) => {
                                formDataJSON[key] = value;
                            });
                            // console.log(formDataJSON);

                            const res = await axios({
                                url: "http://localhost:7171/api/save_form_data",
                                method: "POST",
                                data: {
                                    user_id: user_id,
                                    form_id: template_id,
                                    data: formDataJSON,
                                },
                            });
                            if (res.data.success) {
                            }
                            // Perform any additional actions before submitting the form
                            // console.log("Form submitted!", form);

                            // Submit the form
                            // Use additional logic as needed (e.g., AJAX submission)
                            // form.submit();
                        });
                    }
                }
            } catch (error) {
                console.log(error);
            }
        };
        renderPage();
    }, []);

    return (
        <div>
            {/* Render a container for the GrapesJS content */}
            <div id={"grapesjs-container" + template_id}></div>
        </div>
    );
};

export default Hosting;
