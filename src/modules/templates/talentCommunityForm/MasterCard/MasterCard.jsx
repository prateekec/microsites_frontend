import { Image, Flex, Box, Button, FormControl, FormLabel, FormErrorMessage, FormHelperText, Select, Input, Center, Text, Checkbox } from "@chakra-ui/react";

// import "./MasterCard.css";

function MasterCard() {
    return (
        <>
            <Flex>
                <Box>
                    <MasterCardDesign></MasterCardDesign>
                </Box>
            </Flex>
        </>
    );
}
export const MasterCardDesign = () => {
    const countries = [
        { value: "us", label: "United States" },
        { value: "ca", label: "Canada" },
        { value: "in", label: "India" },
        // Add more countries as needed
    ];
    const regions = [
        { value: "na", label: "North America" },
        { value: "eu", label: "Europe" },
        { value: "as", label: "Asia" },
        // Add more regions as needed
    ];
    return (
        <>
            <Flex justifyContent={"center"}>
                <Flex role="header-inner" flexBasis={"65%"} justifyContent={"space-between"} alignItems={"center"}>
                    <Flex role="left">
                        <Box>
                            <Image src="https://cdn.phenompeople.com/CareerConnectResources/MASRUS/images/Header-1650989573515.png" height={"70px"} width={"100px"}></Image>
                        </Box>
                    </Flex>
                    <Flex role="right" gap={"1rem"} color={"#595959"} fontSize={"14px"} fontWeight={"600"}>
                        <a href="#">
                            <div className="tab">Company Website</div>
                        </a>
                        <a href="#">
                            <div className="tab">Careers</div>
                        </a>
                    </Flex>
                </Flex>
            </Flex>
            <Box position="relative">
                <Image
                    src={"https://cdn.phenompeople.com/CareerConnectResources/MASRUS/images/MastercardTalentCommunity-1696612678322.jpg"}
                    alt="Image"
                    width="100%"
                    height="auto"
                />
                <Box
                    position="absolute"
                    top="0"
                    left="0"
                    width="100%"
                    height="100%"
                    backgroundColor="rgba(0, 0, 0,0.5)" // Adjust the opacity by changing the last value (0.5 is an example)
                />

                <Center
                    position="absolute"
                    top="50%"
                    left="50%"
                    transform="translate(-50%, -50%)"
                    textAlign="center"
                    color="white" // Change text color as needed
                >
                    <Flex width={"62%"} flexDir={"column"}>
                        <Text fontSize="2.5rem">Join our talent community</Text>
                        <Text fontSize={"1.2rem"}>
                            We connect people to Priceless possibilities. Discover yours with a career at Mastercard.
                            <br />
                            <br /> Sign-up for our talent community and get updates on job opportunities, news, and recruiting events at Mastercard.
                        </Text>
                    </Flex>
                </Center>
            </Box>

            <Flex justifyContent={"center"} padding={"4rem 0"}>
                <Flex flexBasis={"65%"}>
                    <FormControl>
                        <Box>
                            <Text fontSize={"30px"} fontWeight={"400"} mb={"1rem"}>
                                Join our Talent Community
                            </Text>
                            <Box boxShadow={"0 0 8px #d4d4d4"} border={"1px solid #d4d4d4"} width={"100%"} minH={"20vh"} padding={"1rem"}>
                                <Text mb={"3rem"} fontSize={"medium"}>
                                    Make completing your job application easier by uploading your resume or CV.
                                </Text>
                                <Text mb={"2rem"} fontSize={"medium"}>
                                    Upload either DOC, DOCX, PDF, or TXT file types (3MB max)
                                </Text>

                                <FormLabel textAlign={"center"} htmlFor="resume">
                                    <Text
                                        display={"inline"}
                                        padding={"0.8rem 2rem"}
                                        fontSize={"14px"}
                                        borderRadius={"65px"}
                                        color={"white"}
                                        border={"2px solid #d52b1e"}
                                        background={"#FF671B"}
                                    >
                                        Upload Resume
                                    </Text>
                                </FormLabel>
                                <Input id="resume" type="file" display={"none"}></Input>
                            </Box>
                        </Box>
                        <Flex width={"100%"} justifyContent={"space-between"} m={"3rem 0"}>
                            <Flex role="left fields" flexDirection={"column"} flexBasis={"45%"} gap={"1rem"}>
                                <Box>
                                    <FormLabel>First Name*</FormLabel>
                                    <Input border={"1px solid #E7E7E7"} type="text" />
                                </Box>
                                <Box>
                                    <FormLabel>Email*</FormLabel>
                                    <Input border={"1px solid #E7E7E7"} type="email" />
                                </Box>
                                <Box>
                                    <FormLabel>Location/Country*</FormLabel>
                                    <Select color={"#777777"} border={"1px solid #E7E7E7"} placeholder="Select a country">
                                        {countries.map((country) => (
                                            <option key={country.value} value={country.value}>
                                                {country.label}
                                            </option>
                                        ))}
                                    </Select>
                                </Box>
                                <Box>
                                    <FormLabel>City*</FormLabel>
                                    <Input border={"1px solid #E7E7E7"} type="text" />
                                </Box>
                            </Flex>
                            <Flex role="right fields" flexDirection={"column"} flexBasis={"45%"} gap={"1rem"}>
                                <Box>
                                    <FormLabel>Last Name*</FormLabel>
                                    <Input border={"1px solid #E7E7E7"} type="text" />
                                </Box>
                                <Box>
                                    <FormLabel>Phone Number</FormLabel>
                                    <Input border={"1px solid #E7E7E7"} type="tel" />
                                </Box>
                                <Box>
                                    <FormLabel>State*</FormLabel>
                                    <Select color={"#777777"} border={"1px solid #E7E7E7"} placeholder="Select Region">
                                        {regions.map((region) => (
                                            <option key={region.value} value={region.value}>
                                                {region.label}
                                            </option>
                                        ))}
                                    </Select>
                                </Box>
                                <Box>
                                    <FormLabel>Area of Interest*</FormLabel>
                                    <Select color={"#777777"} border={"1px solid #E7E7E7"} placeholder="Please Select">
                                        {regions.map((region) => (
                                            <option key={region.value} value={region.value}>
                                                {region.label}
                                            </option>
                                        ))}
                                    </Select>{" "}
                                </Box>
                            </Flex>
                        </Flex>
                        <Flex fontSize={"16px"} flexDir={"column"} gap={"1rem"}>
                            <Text>
                                I understand that Mastercard International Inc. and its affiliates ("Mastercard") will use my personal information as described in the Mastercard
                                Talent Community Privacy Notice..
                            </Text>
                            <Text>I agree that Mastercard may:</Text>
                            <Checkbox border={"#4F4F4F"}>
                                *(Required) Use my personal information to send me personalized communications, including about job opportunities and recruitment events, as further
                                described in the Notice.
                            </Checkbox>
                            <Checkbox border={"#4F4F4F"}>
                                (optional) Use my personal contact information to send me text messages about recruiting events and job opportunities at Mastercard.
                            </Checkbox>
                            <Text>
                                You can withdraw your consent at any time by following the instructions in any of our messages or by contacting us at
                                privacyanddataprotection@mastercard.com.
                            </Text>
                        </Flex>
                        <Button
                            _hover={{ background: "#ffc000" }}
                            display={"inline"}
                            padding={"0.8rem 2rem"}
                            fontSize={"14px"}
                            borderRadius={"65px"}
                            color={"white"}
                            background={"#FF671B"}
                            mt={"1rem"}
                        >
                            Submit
                        </Button>
                    </FormControl>
                </Flex>
            </Flex>
        </>
    );
};
export default MasterCard;
