import { Flex, Button, FormControl, FormLabel, FormErrorMessage, FormHelperText, Input } from "@chakra-ui/react";

import "./Otis.css";

function Otis() {
    return (
        <>
            <div className="container-outer">
                <div className="container-inner">
                    <OtisDesign></OtisDesign>
                </div>
            </div>
        </>
    );
}
export const OtisDesign = () => {
    return (
        <>
            <div className="header-outer">
                <div className="header-inner">
                    <div className="left">
                        <div className="company-logo">
                            <img src="https://otis.avature.net/portal/t24/images/logo.png"></img>
                        </div>
                    </div>
                    <div className="right">
                        <a href="#">
                            <div className="tab">Company Website</div>
                        </a>
                        <a href="#">
                            <div className="tab">Careers</div>
                        </a>
                    </div>
                </div>
            </div>
            <div className="company-banner">
                <img src="https://otis.avature.net/portal/t24/images/banner--main.jpg"></img>
            </div>
            <Flex justifyContent={"space-between"}>
                <div className="community-content">
                    <div className="content-heading">Talent Community</div>
                    <div className="content-article">
                        When you join Otis, you become part of a respected team with a proven ability to develop, build and succeed for generations. You belong to a trusted, caring
                        community where your contributions, and the skills and capabilities you’ll gain working alongside the best and brightest, will keep us connected and on the
                        cutting edge. <br />
                        You can help us push the boundaries of what's possible to thrive in a taller, faster, smarter world.
                        <br /> Ready to get started? Join our Talent Community today, and we’ll keep you up-to-date on exciting career opportunities that match your skillset and
                        interests!
                    </div>
                    <div className="content-heading">#BuildWhatsNext</div>
                </div>
                <Flex flexBasis={"50%"}>
                    <div className="community-form">
                        <div className="form-field">
                            <FormLabel>First Name *</FormLabel>
                            <Input type="text" />
                        </div>
                        <div className="form-field">
                            <FormLabel>Last Name *</FormLabel>
                            <Input type="text" />
                        </div>
                        <div className="form-field">
                            <FormLabel>Email address *</FormLabel>
                            <Input type="email" />
                        </div>
                        <div className="form-field">
                            <FormLabel>Phone number *</FormLabel>
                            <Input type="tel" />
                        </div>
                        <div className="user-policy">
                            I have read the Privacy Notice and Terms of Use of Otis and its affiliates and subsidiaries. By joining the Otis Talent Community, I understand that my
                            personal information may be transferred within Otis, including to its headquarters in the United States, and to third parties selected by Otis who
                            assist Otis in its talent acquisition activities. I further understand that I may receive emails from time to time and that I may unsubscribe from those
                            emails at any time by clicking on the unsubscribe link at the bottom of the email.
                        </div>
                        <Button colorScheme="blue">Submit</Button>
                    </div>
                </Flex>
            </Flex>
        </>
    );
};
export default Otis;
