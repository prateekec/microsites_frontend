import React, { useEffect } from "react";
import axios from "axios";

const WordPress = () => {
    useEffect(() => {
        const fetchPageContent = async () => {
            try {
                const response = await axios.get("http://localhost/wordpressTesting/form-testing/");

                let filteredData = response.data.replace(`action="/wordpressTesting/form-testing/"`, `action="http://localhost:7171/api/save_user_template"`);

                // Replace the entire document with the fetched HTML
                document.open();
                document.write(filteredData);
                document.close();
            } catch (error) {
                console.error("Error fetching WordPress page:", error.message);
            }
        };

        fetchPageContent();
    }, []);

    return null; // This component doesn't render anything
};

export default WordPress;
