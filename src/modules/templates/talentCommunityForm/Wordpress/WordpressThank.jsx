import React, { useEffect } from "react";
import axios from "axios";

const WordpressThank = () => {
    useEffect(() => {
        const fetchPageContent = async () => {
            try {
                const response = await axios.get("http://localhost/wordpressTesting/elementor-268/");

                document.open();
                document.write(response.data);
                document.close();
            } catch (error) {
                console.error("Error fetching WordPress page:", error.message);
            }
        };

        fetchPageContent();
    }, []);

    return null; // This component doesn't render anything
};

export default WordpressThank;
